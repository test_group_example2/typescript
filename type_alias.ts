type CarYear = number
type CarType = string
type CarModel = string
type Car = {
  year: CarYear,
  type: CarType,
  model: CarModel
}

const carYear: CarYear = 2001
const carType: CarType = "Nisan"
const carModel: CarModel = "Corolla"
const car1: Car = {
  year: 2001,
  type: "skyline",
  model: "GT"
};

console.log(carYear,carType,carModel);